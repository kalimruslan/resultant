package ru.ruslan.resultant.fragments.main;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import io.reactivex.Observable;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import ru.ruslan.resultant.models.ResponceStocks;
import ru.ruslan.resultant.models.Stock;
import ru.ruslan.resultant.rest.ApiService;

/**
 * Created by Ruslan Kalimullin on 03.02.2018.
 */
public class MainPresenterTest {

  List<Stock> mStocks;
  @Mock
  private MainPresenter mPresenter;
  @Mock
  private ApiService mApiService;
  @Mock
  private IMainView mView;

  /**
   * Метод запускаемы каждый раз при запуске тестов. Инициализация всех mock объектов,
   * настройка хуков на тестовые события для работы с сетью с помощью RXJava
   */
  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    RxJavaPlugins.setComputationSchedulerHandler(scheduler -> scheduler);
    RxJavaPlugins.setIoSchedulerHandler(scheduler -> Schedulers.trampoline());
    RxJavaPlugins.setNewThreadSchedulerHandler(scheduler -> Schedulers.trampoline());

    RxAndroidPlugins
        .setInitMainThreadSchedulerHandler(rxAndroidScheduler -> Schedulers.trampoline());
    mPresenter = new MainPresenter(mView);
  }

  /**
   * Запускается при завершении тестов. Тут идет очистка всех instance.
   */
  @After
  public void tearDown() throws Exception {
    mPresenter = null;
    RxJavaPlugins.reset();
    RxAndroidPlugins.reset();
  }

  /**
   * Проверяем получение данных, если есть интернет
   * @throws Exception
   */
  @Test
  public void getAllStocksFromServerIfHasInet() throws Exception {
    when(mApiService.getAllStocks())
        .thenReturn(Observable.just(mock(ResponceStocks.class)));
    mPresenter.getAllStocksFromServer(true);
    verify(mView, times(1)).showProgress();
    verify(mView, times(1)).hideProgress();
    verify(mView).populateAdapter(Mockito.anyList());
    verify(mView, never()).showNoInternetConnection();
  }

  /**
   * Проверяем получение данных, если нет интернета
   * @throws Exception
   */
  @Test
  public void getAllStocksFromServerIfHasNotInet() throws Exception {
    when(mApiService.getAllStocks())
        .thenReturn(Observable.just(mock(ResponceStocks.class)));
    mPresenter.getAllStocksFromServer(false);
    verify(mView, never()).showProgress();
    verify(mView, never()).hideProgress();
    verify(mView, never()).populateAdapter(Mockito.anyList());
    verify(mView, times(1)).showNoInternetConnection();
  }

  /**
   * Проверяем, запускатся ли у вью метод получения списка заявок
   */
  @Test
  public void onResume() throws Exception {
    verifyZeroInteractions(mView);
    mPresenter.onResume();
    verify(mView).postHandler();
    verifyNoMoreInteractions(mView);
  }

  /**
   * Проверяем, удаляются ли все каллбеки, когда окно заявок не на переднем плане
   */
  @Test
  public void onPause() throws Exception {
    verifyZeroInteractions(mView);
    mPresenter.onPause();
    verify(mView).removeHandler();
    verifyNoMoreInteractions(mView);
  }

}