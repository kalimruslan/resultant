package ru.ruslan.resultant.rest;

import io.reactivex.Observable;
import java.util.List;
import retrofit2.http.GET;
import ru.ruslan.resultant.BuildConfig;
import ru.ruslan.resultant.models.ResponceStocks;
import ru.ruslan.resultant.models.Stock;

/**
 * Created by Ruslan Kalimullin on 02.02.2018.
 */

public interface IApiService {

  @GET("stocks.json")
  Observable<ResponceStocks> getAllStocks();
}
