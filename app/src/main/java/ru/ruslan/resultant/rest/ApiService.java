package ru.ruslan.resultant.rest;

import io.reactivex.Observable;
import java.util.List;
import ru.ruslan.resultant.models.ResponceStocks;
import ru.ruslan.resultant.models.Stock;

/**
 * Created by Ruslan Kalimullin on 02.02.2018.
 */

public class ApiService implements IApiService {
  @Override
  public Observable<ResponceStocks> getAllStocks() {
    return ApiClient
        .getRestClient()
        .getAllStocks();
  }
}
