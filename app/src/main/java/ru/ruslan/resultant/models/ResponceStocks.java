package ru.ruslan.resultant.models;

import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Created by Ruslan Kalimullin on 03.02.2018.
 */

public class ResponceStocks {
  @SerializedName("stock")
  private List<Stock> mStocks;

  @SerializedName("as_of")
  private String date;

  private String getDate() {
    return date;
  }

  private void setDate(String date) {
    this.date = date;
  }

  public List<Stock> getStocks() {
    return mStocks;
  }

  public void setStocks(List<Stock> stocks) {
    this.mStocks = stocks;
  }
}
