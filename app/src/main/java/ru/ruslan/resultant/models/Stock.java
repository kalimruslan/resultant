package ru.ruslan.resultant.models;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Created by Ruslan Kalimullin on 02.02.2018.
 */

public class Stock implements Parcelable {
  @SerializedName("name")
  private String mName;
  @SerializedName("price")
  private Price mPrice;
  @SerializedName("percent_change")
  private double mPercentageChange;
  @SerializedName("volume")
  private int mVolume;
  @SerializedName("symbol")
  private String mSymbol;

  protected Stock(Parcel in) {
    mName = in.readString();
    mPercentageChange = in.readDouble();
    mVolume = in.readInt();
    mSymbol = in.readString();
  }

  public static final Creator<Stock> CREATOR = new Creator<Stock>() {
    @Override
    public Stock createFromParcel(Parcel in) {
      return new Stock(in);
    }

    @Override
    public Stock[] newArray(int size) {
      return new Stock[size];
    }
  };

  public String getName() {
    return mName;
  }

  public void setName(String mName) {
    this.mName = mName;
  }

  public Price getPrice() {
    return mPrice;
  }

  public void setPrice(Price mPrice) {
    this.mPrice = mPrice;
  }

  public double getPercentageChange() {
    return mPercentageChange;
  }

  public void setPercentageChange(double mPercentageChange) {
    this.mPercentageChange = mPercentageChange;
  }

  public int getVolume() {
    return mVolume;
  }

  public void setVolume(int mVolume) {
    this.mVolume = mVolume;
  }

  public String getSymbol() {
    return mSymbol;
  }

  public void setSymbol(String mSymbol) {
    this.mSymbol = mSymbol;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(mName);
    dest.writeDouble(mPercentageChange);
    dest.writeInt(mVolume);
    dest.writeString(mSymbol);
  }
}
