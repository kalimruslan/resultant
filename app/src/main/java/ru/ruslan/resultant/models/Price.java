package ru.ruslan.resultant.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ruslan Kalimullin on 02.02.2018.
 */

public class Price {

  /**
   * currency : PHP
   * amount : 19.6
   */

  @SerializedName("currency")
  private String mConcurency;
  @SerializedName("amount")
  private double mAmount;

  public String getConcurency() {
    return mConcurency;
  }

  public void setConcurency(String mConcurency) {
    this.mConcurency = mConcurency;
  }

  public double getAmount() {
    return mAmount;
  }

  public void setAmount(double mAmount) {
    this.mAmount = mAmount;
  }
}
