package ru.ruslan.resultant.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;
import ru.ruslan.resultant.R;
import ru.ruslan.resultant.models.Stock;

/**
 * Created by Ruslan Kalimullin on 03.02.2018.
 */

public class StockAdapter extends Adapter<StockViewHolder> {


  private Context mContext;
  private List<Stock> mStocks;
  private LayoutInflater mInflater;

  public StockAdapter(Context context, List<Stock> stocks) {
    mInflater = LayoutInflater.from(context);
    mContext = context;
    mStocks = stocks;
  }

  @Override
  public StockViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = mInflater.inflate(R.layout.item_stock, parent, false);

    return new StockViewHolder(view);
  }

  @Override
  public void onBindViewHolder(StockViewHolder holder, int position) {
    Stock stock = mStocks.get(position);

    holder.tvStockName.setText(stock.getName());
    holder.tvStockVolume.setText(Integer.toString(stock.getVolume()));
    holder.tvStockAmount.setText(String.format("%.2f", stock.getPrice().getAmount()));
  }

  @Override
  public int getItemCount() {
    return mStocks.size();
  }
}
