package ru.ruslan.resultant.adapters;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.ruslan.resultant.R;

/**
 * Created by Ruslan Kalimullin on 03.02.2018.
 */

public class StockViewHolder extends ViewHolder {

  @BindView(R.id.tv_stock_name)
  TextView tvStockName;
  @BindView(R.id.tv_stock_volume)
  TextView tvStockVolume;
  @BindView(R.id.tv_stock_amount)
  TextView tvStockAmount;

  public StockViewHolder(View itemView) {
    super(itemView);
    ButterKnife.bind(this, itemView);
  }
}
