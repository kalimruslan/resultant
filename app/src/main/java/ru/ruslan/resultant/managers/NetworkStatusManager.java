package ru.ruslan.resultant.managers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Created by Asus on 29.09.2016.
 * Проверка интернет и WiFi соединения
 */

public class NetworkStatusManager {

  public static boolean isNetworkAvailable(Context context) {
    try {
      ConnectivityManager connectivityManager =
          (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
      NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
      return networkInfo != null && networkInfo.isConnectedOrConnecting();
    } catch (NullPointerException e) {
      e.printStackTrace();
      Log.d("connect", "" + e.getMessage());
    }
    return false;
  }
}
