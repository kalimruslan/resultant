package ru.ruslan.resultant.managers;

/**
 * Created by Ruslan Kalimullin on 03.02.2018.
 */

public class ConstantsManager {

  public static final int ONE_SECOND = 1000;
  public static final int FREQ_FOR_QUERY_15_SEC = 15;
  public static final String IS_SHOWED_LAUNCHER = "IS_SHOWED_LAUNCHER";
}
