package ru.ruslan.resultant.fragments.main;

/**
 * Created by Ruslan Kalimullin on 03.02.2018.
 */

public interface IMainPresenter {
  void onResume();

  void onPause();
}
