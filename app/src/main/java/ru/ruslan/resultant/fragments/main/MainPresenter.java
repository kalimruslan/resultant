package ru.ruslan.resultant.fragments.main;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.ruslan.resultant.models.ResponceStocks;
import ru.ruslan.resultant.rest.ApiService;
import ru.ruslan.resultant.rest.IApiService;

/**
 * Created by Ruslan Kalimullin on 02.02.2018.
 */

public class MainPresenter implements IMainPresenter {

  private IMainView mView;
  private IApiService mApiService;
  private Disposable mDisposable;

  public MainPresenter(IMainView view){
    mView = view;
    mApiService = new ApiService();
  }

  protected void getAllStocksFromServer(boolean networkAvailable) {
    if(networkAvailable){
      mView.showProgress();
      mDisposable = mApiService
          .getAllStocks()
          .map(ResponceStocks::getStocks)
          .subscribeOn(Schedulers.io())
          .observeOn(AndroidSchedulers.mainThread())
          .doAfterTerminate(mView::hideProgress)
          .subscribe(stocks -> mView.populateAdapter(stocks), throwable -> {
            throwable.printStackTrace();
            mView.showErrorToast();
          });
    }
    else
      mView.showNoInternetConnection();
  }

  @Override
  public void onResume() {
    mView.postHandler();
  }

  @Override
  public void onPause() {
    if(mDisposable != null && !mDisposable.isDisposed())
      mDisposable.dispose();
    mView.removeHandler();
  }
}
