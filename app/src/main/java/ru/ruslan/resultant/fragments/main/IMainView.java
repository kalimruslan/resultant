package ru.ruslan.resultant.fragments.main;

import java.util.List;
import ru.ruslan.resultant.models.Stock;

/**
 * Created by Ruslan Kalimullin on 02.02.2018.
 */

public interface IMainView {

  public void populateAdapter(List<Stock> stocks);

  public void showErrorToast();

  public void showNoInternetConnection();

  void postHandler();

  void removeHandler();

  void showProgress();

  void hideProgress();
}
