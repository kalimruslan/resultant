package ru.ruslan.resultant.fragments.main;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import java.util.List;
import ru.ruslan.resultant.R;
import ru.ruslan.resultant.adapters.StockAdapter;
import ru.ruslan.resultant.fragments.launch.LaunchFragment;
import ru.ruslan.resultant.managers.ConstantsManager;
import ru.ruslan.resultant.managers.NetworkStatusManager;
import ru.ruslan.resultant.models.Stock;

public class MainActivity extends AppCompatActivity implements IMainView,
    OnNavigationItemSelectedListener{

  FragmentManager mFragmentManager;
  MainPresenter mPresenter;
  @BindView(R.id.toolbar)
  Toolbar toolbar;
  @BindView(R.id.rv_stocks)
  RecyclerView rvStocks;
  @BindView(R.id.tv_no_stocks)
  TextView tvNoStocks;
  @BindView(R.id.toolbar_progress_bar)
  ProgressBar progressBar;
  @BindView(R.id.navigation_view)
  NavigationView navigViewTaskRun;
  @BindView(R.id.drawer_layout)
  DrawerLayout drawerLayout;
  ImageButton buttonCloseDrawer;


  private Unbinder mUnbinder;
  private StockAdapter mStockAdapter;
  private Handler mHandler = new Handler();
  private Runnable mGetAllStocks = new Runnable() {
    @Override
    public void run() {
      mPresenter.getAllStocksFromServer(NetworkStatusManager.isNetworkAvailable(getApplicationContext()));
      mHandler
          .postDelayed(this, ConstantsManager.ONE_SECOND * ConstantsManager.FREQ_FOR_QUERY_15_SEC);
    }
  };

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    mUnbinder = ButterKnife.bind(this);
    mFragmentManager = getSupportFragmentManager();
    mPresenter = new MainPresenter(this);
    if(savedInstanceState == null)
      runLaunchScreen();
    initToolbar();
  }

  private void initToolbar() {
    setSupportActionBar(toolbar);
    getSupportActionBar().setTitle(R.string.stocks);
    toolbar.setTitleTextColor(getResources().getColor(R.color.white));
    navigViewTaskRun.setNavigationItemSelectedListener(this);
    buttonCloseDrawer = navigViewTaskRun.getHeaderView(0)
        .findViewById(R.id.button_close_navig_drawer);
    buttonCloseDrawer
        .setOnClickListener(view -> drawerLayout.closeDrawer(GravityCompat.END));
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    outState.putBoolean(ConstantsManager.IS_SHOWED_LAUNCHER, true);
    super.onSaveInstanceState(outState);
  }

  private void runLaunchScreen() {
    LaunchFragment launchFragment = new LaunchFragment();

    mFragmentManager
        .beginTransaction()
        .replace(R.id.content_farme, launchFragment)
        .addToBackStack(null)
        .commit();
  }

  @Override
  protected void onResume() {
    super.onResume();
    mPresenter.onResume();
  }

  @Override
  protected void onPause() {
    super.onPause();
    mPresenter.onPause();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if(mUnbinder != null)
      mUnbinder.unbind();
  }

  @Override
  public void populateAdapter(List<Stock> stocks) {
    mStockAdapter = new StockAdapter(this, stocks);
    LinearLayoutManager llm = new LinearLayoutManager(this,
        LinearLayoutManager.VERTICAL, false);
    rvStocks.setLayoutManager(llm);
    rvStocks.setAdapter(mStockAdapter);
  }

  @Override
  public void showErrorToast() {
    Toast.makeText(this, R.string.toast_no_data, Toast.LENGTH_SHORT).show();
  }

  @Override
  public void showNoInternetConnection() {
    Toast.makeText(this, R.string.toast_no_internet_connection, Toast.LENGTH_SHORT).show();
  }

  @Override
  public void postHandler() {
    mHandler.post(mGetAllStocks);
  }

  @Override
  public void removeHandler() {
    mHandler.removeCallbacks(mGetAllStocks);
  }

  @Override
  public void showProgress() {
    progressBar.setVisibility(View.VISIBLE);
  }

  @Override
  public void hideProgress() {
    progressBar.setVisibility(View.INVISIBLE);
  }

  @Override
  public boolean onNavigationItemSelected(@NonNull MenuItem item) {
    switch (item.getItemId()) {
      case R.id.navi_update_list:
        mPresenter.getAllStocksFromServer(NetworkStatusManager.isNetworkAvailable(this));
        break;
    }
    DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
    drawerLayout.closeDrawer(GravityCompat.END);
    return true;
  }
}
