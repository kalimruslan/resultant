package ru.ruslan.resultant.fragments.launch;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.concurrent.TimeUnit;
import ru.ruslan.resultant.R;

/**
 * Created by Ruslan Kalimullin on 02.02.2018.
 */

public class LaunchFragment extends Fragment {

  public LaunchFragment(){}

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {

    LaunchTask launchTask = new LaunchTask();
    launchTask.execute();

    return inflater.inflate(R.layout.fragment_launch_screen, container, false);
  }

  class LaunchTask extends AsyncTask<Void, Void, Void> {

    @Override
    protected Void doInBackground(Void... params) {
      try {
        TimeUnit.SECONDS.sleep(5);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

      getActivity().getSupportFragmentManager().popBackStack();

      return null;
    }
  }

}
